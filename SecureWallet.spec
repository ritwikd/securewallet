# -*- mode: python -*-
a = Analysis(['SecureWallet.py'],
             pathex=['/home/ritwik/workspace/securewallet'],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='SecureWallet',
          debug=False,
          strip=None,
          upx=True,
          console=True )
