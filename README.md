SecureWallet password manager Git repository.

The current goal is to reimplement the framework for the application and perhaps give the UI a bit of a facelift. This can be done by generally thinking about the UI, and in case of the framework using PyCrypto to provide a proper cryptographic implementation.

Ritwik Dutta
