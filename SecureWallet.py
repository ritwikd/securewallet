#SecureWallet Password Manager

#GUI import
import wx

#File management imports
from shlex import split
from os import listdir


#Crypto imports
from os import urandom 
from Crypto.Cipher import AES
from hashlib import sha256

#Global vars
userPassList = {}
userPass = ""

#TODO
#============================
#Implement a split view panel 
#   Load stuff on selection
#   

class hashFuncs():
    global userPassList
    global userPass

    def __init__(self):
        pass

    def genIV(self):
        return urandom(16)

    def hashPass(self, userPass):
        return sha256(userPass).digest()

    def encryptText(self, text):
        hashedPass = self.hashPass(userPass)
        randomIV = self.genIV()
        tempEnc = AES.new(hashedPass, AES.MODE_CFB, randomIV)
        cipherText = self.hashPass(hashedPass) + randomIV + tempEnc.encrypt(text)
        del(tempEnc)
        del(randomIV)
        del(hashedPass)
        return cipherText

    def decryptText(self, readIV, cipherText):
        hashedPass = self.hashPass(userPass)
        tempDec = AES.new(hashedPass, AES.MODE_CFB, readIV)
        plainText = tempDec.decrypt(cipherText)
        del(hashedPass)
        del(tempDec)
        return plainText

    def readDat(self):
        if ("securewallet.dat" in listdir(".")):
            datHandler  = open("securewallet.dat", "r")
            datHandler.seek(32)
            readIV = datHandler.read(16)
            cipherText = datHandler.read()
            plainTextLines = self.decryptText(readIV, cipherText).split("\n")
            for i in xrange(0, len(plainTextLines)-2, 2):
                splitLine = split(plainTextLines[i+1])
                if (len(splitLine) > 1):
                    userPassList[plainTextLines[i]] = split(plainTextLines[i+1])
            del(readIV)
            del(plainTextLines)
            datHandler.close()
            del(datHandler)
        else:
            print "No .dat file."


    def writeDat(self):
        datHandler  = open("securewallet.dat", "w+")
        plainText = ""
        for serviceName in userPassList.keys():
            plainText = plainText + serviceName + "\n" + userPassList[serviceName][0] + " " + userPassList[serviceName][1] + "\n"
        cipherText = self.encryptText(plainText)
        datHandler.write(cipherText)
        datHandler.close()
        del(cipherText)
        del(datHandler)
        del(plainText)
    

class passwordDialog(wx.Dialog):
    #Initialize function for dialog
    def __init__(self, parent, text):
        wx.Dialog.__init__(self, parent, 1, "SecureWallet Password Dialog", size=(320,120))
        self.textLine = wx.StaticText(self, -1, text, (10, 5),(300, 30))
        self.passwordBox = wx.TextCtrl(self,wx.ID_OK,"",(10,45),(300,30), style = wx.TE_PASSWORD)
        self.okButton = wx.Button(self,wx.ID_OK,"OK",(100,85),(100,30))

class serviceDialog(wx.Dialog):
    #Initialize function for dialog
    def __init__(self, parent, serviceName, userName, passWord):
        wx.Dialog.__init__(self, parent, 1, "SecureWallet Service Dialog", size=(320,170))
        self.serviceNameBox = wx.TextCtrl(self,wx.ID_OK, serviceName,(10,10),(300,30))
        self.userNameBox = wx.TextCtrl(self,wx.ID_OK, userName,(10,50),(300,30))
        self.passwordBox = wx.TextCtrl(self,wx.ID_OK, passWord,(10,90),(300,30))
        self.okButton = wx.Button(self,wx.ID_OK,"OK",(100,130),(100,30))



class secureWalletFrame(wx.Frame):
    global userPassList
    global userPass

    def __init__(self):
        wx.Frame.__init__(self, None, -1, "SecureWallet", (-1,-1), (800, 600), wx.DEFAULT_FRAME_STYLE)
        self.hashFuncs = hashFuncs()
        self.initAppFramework()
        self.initDisplayElements()


    def getServiceInfo(self, serviceName = "Name of service", userName = "Username", passWord = "Password"):
        servDl = serviceDialog(self, serviceName, userName, passWord)
        if (servDl.ShowModal() == wx.ID_OK):
            dataRet = [servDl.serviceNameBox.GetValue().encode("ascii", "ignore"), servDl.userNameBox.GetValue().encode("ascii", "ignore"), servDl.passwordBox.GetValue().encode("ascii", "ignore")]
            servDl.Destroy()
            del(servDl)
            if ("" in dataRet):
                return None
            else:
                return dataRet
        else:
            return None

    def initApp(self):
        if ("securewallet.dat" in listdir(".")):
            datHandler  = open("securewallet.dat", "r")
            doubleHash = datHandler.read(32)
            datHandler.close()
            del(datHandler)

            dialogInstance = passwordDialog(self, "Enter your password (3 tries):")
            if dialogInstance.ShowModal() == wx.ID_OK:
                userPass = dialogInstance.passwordBox.GetValue().encode("ascii", "ignore")
                dialogInstance.Destroy()
            else:
                dialogInstance.Destroy()

            if (self.hashFuncs.hashPass(self.hashFuncs.hashPass(userPass)) != doubleHash):
                dialogInstance = passwordDialog(self, "Enter your password (2 tries):")
                if dialogInstance.ShowModal() == wx.ID_OK:
                    userPass = dialogInstance.passwordBox.GetValue().encode("ascii", "ignore")
                    dialogInstance.Destroy()
                else:
                    dialogInstance.Destroy()

                if (self.hashFuncs.hashPass(self.hashFuncs.hashPass(userPass)) != doubleHash):
                    dialogInstance = passwordDialog(self, "Enter your password (Last try):")
                    if dialogInstance.ShowModal() == wx.ID_OK:
                        userPass = dialogInstance.passwordBox.GetValue().encode("ascii", "ignore")
                        dialogInstance.Destroy()
                    else:
                        dialogInstance.Destroy()

                    if (self.hashFuncs.hashPass(self.hashFuncs.hashPass(userPass)) != doubleHash):
                        return False
                    else:
                        self.hashFuncs.readDat()
                        return True
                else:
                    self.hashFuncs.readDat()
                    return True
            else:
                self.hashFuncs.readDat()
                return True
            
            
        else:
            dialogInstance = passwordDialog(self, "Enter a password for encrypting your data:")
            if dialogInstance.ShowModal() == wx.ID_OK:
                userPass = dialogInstance.passwordBox.GetValue().encode("ascii", "ignore")
                dialogInstance.Destroy()
            else:
                dialogInstance.Destroy()
            self.hashFuncs.writeDat()
            return True
        
    def initAppFramework(self):
        if (self.initApp() == True):
            print "Successfully intialized."
            print userPassList
            self.Show(True)
        else:
            print "Bad password."
            wx.MessageBox("You have entered an incorrect password 3 times. ", "Error", 
            wx.OK | wx.ICON_ERROR)
            self.Destroy()

    def initDisplayElements(self):
        self.menuBar = wx.MenuBar()
        
        self.fileMenu = wx.Menu()
        self.Bind(wx.EVT_MENU, self.onQuit, self.fileMenu.Append(wx.ID_EXIT, "Quit", "Quit the app."))
        self.Bind(wx.EVT_MENU, self.onAdd, self.fileMenu.Append(wx.ID_ADD, "&Add\tCtrl+N", "Add an item."))
        self.Bind(wx.EVT_MENU, self.onDelete, self.fileMenu.Append(wx.ID_DELETE, "&Delete\tCtrl+D", "Delete the currently selected item."))
        self.Bind(wx.EVT_MENU, self.onEdit, self.fileMenu.Append(wx.ID_EDIT, "&Edit\tCtrl+E", "Edit the currently selected item."))
        self.menuBar.Append(self.fileMenu, "&File")
        self.SetMenuBar(self.menuBar)

        self.splitterWindow = wx.SplitterWindow(self, -1)
        self.listView =wx.ListBox(self.splitterWindow, -1, wx.DefaultPosition, wx.DefaultSize, \
            userPassList.keys(), wx.LB_SINGLE)  
        self.listView.Bind(wx.EVT_KEY_DOWN, self.onKeyPress)
        self.infoArea = wx.TextCtrl(self.splitterWindow, 2, "Choose a service to display its information.", \
            wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY|wx.TE_MULTILINE) 
        self.splitterWindow.SplitVertically(self.listView, self.infoArea)
        self.Bind(wx.EVT_LISTBOX, self.onSelect, self.listView)


    def onKeyPress(self, event):
        if (event.GetKeyCode() == wx.WXK_DELETE):
            self.onDelete(None)
    


    def onAdd(self, event):
        serviceData = self.getServiceInfo()
        if (serviceData != None):
            userPassList[serviceData[0]] = [serviceData[1], serviceData[2]]
            print userPassList
            self.hashFuncs.writeDat()
            self.hashFuncs.readDat()
            self.listView.SetItems(userPassList.keys())
        
    def onEdit(self, event):
        serviceName = self.listView.GetString(self.listView.GetSelection())
        serviceArr = userPassList[serviceName]
        serviceData = self.getServiceInfo(serviceName, serviceArr[0], serviceArr[1])
        if (serviceData != None):
            userPassList[serviceData[0]] = [serviceData[1], serviceData[2]]
            self.hashFuncs.writeDat()
            self.hashFuncs.readDat()
            self.onSelect(None)

    def onDelete(self, event):
        serviceName = self.listView.GetString(self.listView.GetSelection())
        del(userPassList[serviceName])
        self.hashFuncs.writeDat()
        self.hashFuncs.readDat()
        self.listView.SetItems(userPassList.keys())
        self.infoArea.SetValue("")

    def onQuit(self, event):
        self.Destroy()

    def onSelect(self, event):
        serviceName = self.listView.GetString(self.listView.GetSelection())
        serviceArr = userPassList[serviceName]
        self.infoArea.SetValue(str(serviceName) + "\n" + "Username:\n" + serviceArr[0] + "\n" + "Password:\n" + serviceArr[1])


class secureWalletApp(wx.App):
    def OnInit(self):
        #Create instance of frame and start loop
        secureWalletFrameInstance = secureWalletFrame()
        return True

secureWalletRunningInstance = secureWalletApp(0)
secureWalletRunningInstance.MainLoop()
